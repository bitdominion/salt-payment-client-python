============
Installation
============

At the command line::

    $ easy_install salt-payment-client-python

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv salt-payment-client-python
    $ pip install salt-payment-client-python