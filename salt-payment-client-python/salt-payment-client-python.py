class PaymentRequest(object):
    
    # ==== Property: Invoice ====
    
    @property
    def invoice(self):
        """
        Unique identifier for this payment request. Required to submit the payment request. You may also call `set_random_invoice` to generate a random identifier.
        """
        return getattr(self, "_invoice", None)
    
    @property.setter
    def invoice(self, v):
        self._invoice = v
        
    def set_random_invoice(self):
        pass
    
    # ==== Property: Language ====
    
    @property
    def language(self):
        """
        Language for the payment page. Allowed values: 'en', 'fr', 'zh_CN', 'zh_TW', 'es'
        """
        return getattr(self, "_language", "en")
    
    @property.setter
    def language(self, v):
        if not v in ("en", "fr", "zh_CN", "zh_TW", "es"):
            raise ValueError("Unknown language code %s, allowed: 'en', 'fr', 'zh_CN', 'zh_TW', 'es'")
        self._language = v
        
    # ==== Methods ====
        
    def validate(self):
        """
        Some validations are handled in the property setters, but more complicated validations are checked by the method. Call this method prior to generating the HTML.
        """
        pass
    
    def generate_html(self):
        """
        Generates an HTML fragment containing an auto-posting form to send the payment request to the payment processor.
        """
        pass